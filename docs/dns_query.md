# DNS Query

Every request for getting `IP Address` of a `Domain Name` called a `DNS Query` that will send over `DNS Protocol (Port: 53)`

> [!TIP]
> DNS Port: `53`
> DNS Protocols:
>
> 1. **TCP** => For **big** queries
> 2. **UDP** => For **small** queries

We can categorize `DNS Queries` based on three params into groups:

1. **Group 1**:
    1. Internal Query
    2. External Query
2. **Group 2**:
    1. Iterative Query
    2. Recursive Query
3. **Group 3**:
    1. Forward Lookup Query
    2. Reverse Lookup Query

## Group 1

When we create a DNS query:

1. Check `Local Hosts File` (Cached version) (Internal)
2. If not found:
    1. Check `Query Histories` (Cached history) (Internal)
    2. If not found:
        1. Send query to `DNS Server` (External)

### Internal DNS Query

First of all OS tries to find `DNS Result` at the local system

#### Local Hosts File (Cached Version)

Maps `One Host` to `One IP` per line:

```code
127.0.0.1	localhost
127.0.1.1	koliber

# The following lines are desirable for IPv6 capable hosts
::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

##### Windows

**Address**: `C:\Windows\System32\drivers\etc\hosts`

##### Mac

**Address**: `/private/etc/hosts`

##### Linux

**Address**: `/etc/hosts`

> [!TIP]
> After every change on `/hosts` file we must flush `Local Cache` for apply the changes and reloading the file into cache

#### Query Histories (Cached Histories)

This file will manage automatically

---

### External DNS Query

If `DNS Result` doesn't found at the local system OS will send query to `DNS Server` defained at interface configs

#### DNS Server Query

OS will send a `DNS Query` for and specific `Record` to DNS Server

---

## Group 2

When our query sent to the `External DNS Server` it will response in two ways:

1. It sends query to another DNS server and send's back the response to client
2. It send query to another DNS server and follow the responses until gets IP address then send's back the response to client

![Iterative vs Recursive](assets/recursive-iterative-dns-query-ITNibble.png)

### Iterative DNS Query

A `single request-response` DNS Query called `Iterative Query`

### Recursive DNS Query

A `single request` and the multi req-res per that to get valid `IP` called `Recursive Query`

---

## Group 3

DNS Queries are two type:

1. `Domain Name` => `IP Address` (Forward Lookup)
2. `IP Address` => `Domain Name` (Reverse Lookup)

### Forward Lookup DNS Query

`Domain Name` --> `IP Address`

### Reverse Lookup DNS Query

`IP Address` --> `Domain Name`

---
