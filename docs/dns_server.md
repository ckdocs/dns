# DNS Server

## Servers Hierarchy

![Hierarchy](assets/dns_1.png)

![Hierarchy 2](assets/fwd-delegation.png)

### Root DNS Server (Root Hint) (13 Servers)

There are `13` main Root Servers that at first every DNS Query will send to them, then the response contains `NS Record` of the `TLD DNS Server` to request to that sub level server

> [!TIP]
> DNS Servers contains `NS Record` of `TLD DNS Servers` and `Domain Names`
>
> `13 Servers` in the world

### TLD DNS Server (Top Level Domain) (Country `NIC` Servers => .ir, .com, ...)

> [!TIP]
> DNS Servers contains `NS Record` of `SLD DNS Servers` and `Domain Names`
>
> `One or more Servers` (`NIC` Server) per `.ir`, `.com`, `.org`, ...
>
> The public `IANA` standard for naming and managing TLD's

### SLD DNS Server (Second Level Domain) (Companies => Mihanwebhost, Dynu, ...)

> [!TIP]
> DNS Servers contains `A, CNAME Record` of `Our Server` and `Domain Names`
>
> `Valid country hosing company DNS Servers`: `Mihanwebhost`, `Dynu`, ...

### Subdomain DNS Server (Sub Domain) (Our Server)

> [!TIP]
> Our server can act as `DNS Server` to routing `Subdomain` of our `Domain Name`
>
> Using `Bind`, `Windows DNS Server`, ...

---

## Servers Types

![Server Types](assets/dns-security-1.png)

### Primary (Master) (Read-Write Zone)

Creates a copy of a `Zone File` that can be updated directly on this server

![Primary vs Secondary](assets/12345.png)

---

### Secondary (Slave) (Read-Only Zone)

Creates a copy of a `Zone File` that exists on another server for load balancing on a primary server

#### Zone Transfer (IXFR Query)

Is a huge `TCP` DNS Query that every `Secondary` sends to `Primary` for loading the entire `Zone File`

![IXFR](assets/ixfr.png)

![AXFR](assets/master-slave-zone-transfer.png)

---

### Cache

Is a `DNS Server` that will cache all of the Queries for optimizing performance, like:

1. `ISP` DNS Server
2. `Google` DNS Server (`8.8.8.8`)
3. `Cloudflare` DNS Server (`1.1.1.1`)
4. etc

---

### Stub (Forwarder)

Creates a copy of a `Zone File` containing only Name Servers (`NS Records`), Start Of Authority (`SOA Record`), Glue Hosts (`A Record`)

> [!NOTE]
> Our local PC can act as `Stub` cache DNS Server for optimizing performance

#### Conditional Forwarder

A Conditional forwarder is a `Stub DNS Server` that only contains some of `NS Records` contains a condition (Not all), for example only `.ir` records or etc.

---
