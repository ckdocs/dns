# DNS Record

DNS Server acts as a `Cache Database` that contains `DNS Records` as `rows of database`, and we send our DNS Query to DNS Server for getting one or more `Records`

Every DNS Record has a type:

1. A: `Domain` => `IP`
2. PTR: `IP` => `Domain`
3. CNAME: `Domain` => `Domain` (Alias - Redirect)
4. MX: `Domain` => `Main Server`
5. NS: `Domain` => `Another Name Server Link` (Link to another DNS Server to ask)
6. SOA: `Primary DNS Server Info`

Every DNS Record in `Bind DNS Server` has a format:

```code
name    ttl     record_class    record_type     record_data

name: ( ), (@), (Subdomain), (FQDN)
ttl: ( ), (seconds), ...
record_class: (IN), ...
record_type: (SOA), (NS), (A), (AAAA), (PTR), ...
```

Example:

```code
$ORIGIN example.com.     ; designates the start of this zone file in the namespace
$TTL 1h                  ; default expiration time of all resource records without their own TTL value
example.com.  IN  SOA   ns.example.com. username.example.com. ( 2007120710 1d 2h 4w 1h )
example.com.  IN  NS    ns                    ; ns.example.com is a nameserver for example.com
example.com.  IN  NS    ns.somewhere.example. ; ns.somewhere.example is a backup nameserver for example.com
example.com.  IN  MX    10 mail.example.com.  ; mail.example.com is the mailserver for example.com
@             IN  MX    20 mail2.example.com. ; equivalent to above line, "@" represents zone origin
@             IN  MX    50 mail3              ; equivalent to above line, but using a relative host name
example.com.  IN  A     192.0.2.1             ; IPv4 address for example.com
              IN  AAAA  2001:db8:10::1        ; IPv6 address for example.com
ns            IN  A     192.0.2.2             ; IPv4 address for ns.example.com
              IN  AAAA  2001:db8:10::2        ; IPv6 address for ns.example.com
www           IN  CNAME example.com.          ; www.example.com is an alias for example.com
wwwtest       IN  CNAME www                   ; wwwtest.example.com is another alias for www.example.com
mail          IN  A     192.0.2.3             ; IPv4 address for mail.example.com
mail2         IN  A     192.0.2.4             ; IPv4 address for mail2.example.com
mail3         IN  A     192.0.2.5             ; IPv4 address for mail3.example.com
```

---

## DNS Zone File

DNS Server `Database` contains a lot of `Zones` (Or sites), every `Zone` has some `Records`, So DNS Database contains these `Records`

We must define our zone or site records in a new `Zone File` and add it to DNS Server `Zones`

### Example

```code
sudo apt install bind9 bind9utils   # Ubuntu Bind DNS Server Installation
sudo yum install bind bind-utils    # CentOS Bind DNS Server Installation
```

**/etc/bind/bind.conf.options**: (DNS Server config file - Define Zones, Accesses):

```code
zone "example.com" IN {
    type master;
    file "example.com.internal.zone";
};
```

**/var/cache/named/example.com.internal.zone**: (Example.com Site Zone File - Records):

> [!TIP]
>
> `/var/cache/named` folder is defined using `directory` command in `bind.conf.options`

```code
$ORIGIN     example.com.
$TTL        1d
        IN  SOA     ns.example.com. username.example.com. ( 2007120710 1d 2h 4w 1h )
        IN  NS      ns
        IN  A       192.168.1.5
ns      IN  A       192.168.1.5
```

**Check Configs Commands**:

```code
named-checkconf /etc/bind/bind.conf.options
named-checkzone example.com /var/cache/named/example.com.internal.zone
```

**Load Zone Commands**:

```code
sudo service bind9 restart
dig example.com
```

---

## \$TTL Config (Time To Live)

Every record has a `TTL` and if `TTL` ends DNS Server must reload that record form the `Zone File` (If it's primary) or reload from `Master DNS Server` (If it's slave) or reloads from `Another DNS Server` (If it's cache)

```code
$TTL 86400  ; 86400 seconds: 1day

$TTL 1w     ; 1 week

$TTL 1h     ; 1 hour
```

---

## \$ORIGIN Config (Origin)

We can define a default `Origin Domain Name` (`FQDN - Fully Qualified Domain Name - Ends with .`) for making `Record Lines` more less

```code
$ORIGIN koliber.ir.
```

---

## SOA Record (Start Of Authority)

The informations about `Primary DNS Server` and `Serial Number` will define in `SOA Record`

> [!NOTE]
> If `Serial Number` of primary server changes `Secondary DNS Servers` will notify to reload Zone File using `AXFR` query

```code
        IN    SOA    ns1.example.com. hostmaster.example.com. (
            2015081741  ;   serial => data hash (reload data)
            1d          ;   refresh time
            1d          ;   retry time
            1d          ;   expire time
            1d          ;   ttl time
        )

example.com.  IN  SOA   ns.example.com. username.example.com. ( 2007120710 1d 2h 4w 1h )
```

---

## NS Record (Name Server)

Means: if the client requests the `name` then it must ask from another `DNS Server` with address `record_data`

```code
example.com.  IN  NS    ns                    ; ns.example.com is a nameserver for example.com
example.com.  IN  NS    ns.somewhere.example. ; ns.somewhere.example is a backup nameserver for example.com
```

---

## A or AAAA Record (Address)

Means: if the client requests the `name` then answer is `IP Address` in `record_data`

```code
example.com.  IN  A     192.0.2.1             ; IPv4 address for example.com
              IN  AAAA  2001:db8:10::1        ; IPv6 address for example.com
ns            IN  A     192.0.2.2             ; IPv4 address for ns.example.com
              IN  AAAA  2001:db8:10::2        ; IPv6 address for ns.example.com
```

---

## PTR or NAPTR Record (Pointer)

Means if the client requests the `ARPA name (IP Address)` then answer is `Domain Name` in `record_data`

### ARPA

Is a world standard for generating `Domain Names` based on `IP v4, v6 Addresses` for reverse lookup

![ARPA](assets/DNS-tree-showing-reverse-delegations-to-AFRINIC-members.png)

---

## CNAME Record (Canonical Name)

Means if the client requests the `name` then answer is `Domain Name` in `record_data` to redirect the client

```code
www           IN  CNAME example.com.          ; www.example.com is an alias for example.com
wwwtest       IN  CNAME www                   ; wwwtest.example.com is another alias for
              IN  CNAME example2.com.         ; example.com =redirect=> example2.com
```

---

## MX Record (Mail Exchanger)

Means if the client requests the `name main server` then answer is `Main Server Domain Name` in `record_data`

```code
example.com.  IN  MX    10 mail.example.com.  ; mail.example.com is the mailserver for example.com
@             IN  MX    20 mail2.example.com. ; equivalent to above line, "@" represents zone origin
@             IN  MX    50 mail3              ; equivalent to above line, but using a relative host name
```

---

## HINFO Record (Hardware Info)

Means if the client requests the `name hardware info` then answer is `Hardware Info` in `record_data`

```code
example.com.  IN  HINFO    "Dell-Vostro 3500-A"
```

---

## TXT Record (Text)

Means if the client requests the `name additional text` then answer is `Text` in `record_data`

```code
example.com.  IN  TXT    "Silent Hill - Room 304 - Rack 01-5A"
```

---

## RP Record (Responsible Person)

Means the `name` responsible person for auto mail on error on `SOA DNS Server`

```code
example.com.  IN  PR    koliberr136a1@gmail.com
```

---

## Load Balancing

We can balance the load over `Domain` into more servers by defining multiple `A Records` per `One Name`

```code
              IN  A     192.168.1.5
              IN  A     192.168.1.6
              IN  A     192.168.1.7
```

---
