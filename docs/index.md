# Domain Name Service

DNS or `Domain Name Service` is a way to find and `IP` address mapped to a `Domain Name`

![DNS](assets/recursive-resolver.webp)

```code
www.google.com      -->     172.217.169.228
```

---
